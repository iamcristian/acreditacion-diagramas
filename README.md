# Diagramas realizados en plantuml
## para generarlos se utilizó la extensión de VSCode: PlantUML
> [!NOTE] 
> para visualizar directamente los diagramas esta en la carpeta out en formato png
- https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml
- La configuracion de plantuml para visualizar los componentes es la siguiente:
- Se debe tener instalado java y graphviz
- Esta es la pagina para instalar Graphviz: https://graphviz.org/download/
- Y en configuracion de la extension plantuml se debe configurar lo siguiente:
![Alt text](image.png)
- Y con eso se puede previsualizar los diagramas en VSCode y editarlos
![Alt text](image-1.png)
![Alt text](image-2.png)